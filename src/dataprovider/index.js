import todoProvider from "./todoProvider";
const dataprovider = {
    
    getList: (resource, params) => {
        switch (resource) {
            case "todos": return (todoProvider.getList(resource, params));
            default: break;
        }
    },

    getOne: (resource, params) => {
        switch (resource) {
            case "todos": return (todoProvider.getOne(resource, params));
            default: break;
        }
    },

    update: (resource, params) => {
        switch (resource) {
            case "todos": return (todoProvider.update(resource, params));
            default: break;
        }
    },

    create: (resource, params) => {
        switch (resource) {
            case "todos": return (todoProvider.create(resource, params));
            default: break;
        }
    },

    delete: (resource, params) => {
        switch (resource) {
            case "todos": return (todoProvider.delete(resource, params));
            default: break;
        }
    },
};

export default dataprovider