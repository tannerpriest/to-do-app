// @ts-nocheck
import { fetchUtils } from 'react-admin';
require("whatwg-fetch");

let apiUrl = "https://ayqgothi5e.execute-api.us-east-2.amazonaws.com/prod"


const httpClient = fetchUtils.fetchJson;

const todoProvider = {
    getList: (resource, params) => {
        /* Do not delete, will implement filtering soon. */
        // const { page, perPage } = params.pagination;
        // const { field, order } = params.sort;
        // const query = {
        //     sort: JSON.stringify([field, order]),
        //     range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
        //     filter: JSON.stringify(params.filter),
        // };
        const url = `${apiUrl}/${resource}`;
        return httpClient(url).then(({ headers, body }) => {
            const parseData = JSON.parse(body)
            const data = parseData.data || []
            return ({
                data: data,
                total: parseData.total
            })
        });
    },

    getOne: (resource, params) => {
        const url = `${apiUrl}/${resource}/${params.id}`;
        return httpClient(url).then(({ body }) => {
            const data = JSON.parse(body)
            return ({
                data: data
            })
        });
    },

    update: (resource, params) => {
        const url = `${apiUrl}/${resource}/${params.id}`;
        const options = {
            body: JSON.stringify(params.data),
            method: "PUT"
        }
        return httpClient(url, options).then((res) => {
            return ({
                data: params.data
            })
        }).catch((err) => {
            return Promise.reject(err)
        });
    },

    create: (resource, params) => {
        const url = `${apiUrl}/${resource}`;
        const options = {
            body: JSON.stringify(params.data),
            method: "POST"
        }
        return httpClient(url, options).then((res) => {
            return ({
                data: JSON.parse(res.body)
            })
        }).catch((err) => {
            return Promise.reject(err)
        });
    },

    delete: (resource, params) => {
        const url = `${apiUrl}/${resource}/${params.id}`;
        const options = {
            method: "DELETE"
        }
        return httpClient(url, options).then((res) => {
            return ({
                data: {}
            })
        }).catch((err) => {
            return Promise.reject(err)
        });
    },
    deleteMany: async (resource, params) => {
        const options = {
            method: "DELETE"
        }
        for (const id of params.ids) {
            const url = `${apiUrl}/${resource}/${id}`;
            await httpClient(url, options).catch((err) => {
                return Promise.reject(err)
            })
        }
        return Promise.resolve({
            data: params.ids
        })
    },
};

export default todoProvider
