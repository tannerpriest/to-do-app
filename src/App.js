// @ts-nocheck
import React from "react"
import { Admin, Resource } from 'react-admin';
import dataprovider from "./dataprovider";
import todos from "./resources/todos";

function App() {
  return (
    <div className="App">
      <Admin dataProvider={dataprovider}>
        <Resource name="todos" {...todos} />
      </Admin>
    </div >
  );
}

export default App;