// @ts-nocheck
import React from 'react'
import { Create, List, TextField, SelectField, Datagrid, EditButton, ShowButton, Show, SimpleShowLayout, Edit, SimpleForm, TextInput, SelectInput } from 'react-admin';

const CompletedCheckBox = (props) => {
    return (
        <SelectInput
            source="completed"
            choices={completedInputChoices}
            defaultValue={props.record.completed || "false"}
        />
    )
}
const completedInputChoices = [
    {
        id: "true",
        name: "Completed"
    },
    {
        id: "false",
        name: "Not Completed"
    }
]
const completedFieldChoices = [
    {
        id: "true",
        name: "Completed"
    },
    {
        id: "false",
        name: "Not Completed"
    },
    {
        id: undefined,
        name: "Not Completed"
    },
    {
        id: "",
        name: "Not Completed"
    }
]

const TitleField = <TextField source="title" addLabel />
const DetailField = <TextField source="details" addLabel />
const CompletedField = <SelectField source="completed" label="Completed" choices={completedFieldChoices} addLabel />

const TodosList = (props) => {
    return (
        <List {...props}>
            <Datagrid>
                {TitleField}
                {DetailField}
                {CompletedField}
                <EditButton />
                <ShowButton />
            </Datagrid>
        </List>
    )
}
const TodoTitle = ({ record }) => {
    return <span>To Do {record ? `"${record.title}"` : ''}</span>;
};
const TodoShow = (props) => (
    <Show {...props} title={<TodoTitle />}>
        <SimpleShowLayout>
            {TitleField}
            {DetailField}
            {CompletedField}
        </SimpleShowLayout>
    </Show>
)

const ToDoEdit = (props) => (
    <Edit {...props} title={<TodoTitle />} >
        <SimpleForm redirect="show">
            <TextInput source="title" />
            <TextInput source="details" multiline rows={4} />
            <CompletedCheckBox />
        </SimpleForm>
    </Edit>
)

const ToDoCreate = (props) => (
    <Create {...props} title={<TodoTitle />} >
        <SimpleForm redirect="show">
            <TextInput source="title" />
            <TextInput source="details" multiline rows={4} />
            <CompletedCheckBox />
        </SimpleForm>
    </Create>
)

const todos = {
    list: TodosList,
    show: TodoShow,
    edit: ToDoEdit,
    create: ToDoCreate
}

export default todos