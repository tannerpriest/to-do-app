# To Do App

S3 hosted application using React that uses a RESTful API thru AWS Gateway, DynamoDB for data storage and Lambda for backend services. 

You will need an .env file with the variables:

```
REACT_APP_ENVIROMENT=localhost
REACT_APP_PRODUCTION_URL=YOUR_API_ENDPOINT
```

localhost variable will point to a proxy server on port 8010 using a npm package called '[Local CORS Proxy](https://www.npmjs.com/package/local-cors-proxy)'

```
npm install -g local-cors-proxy

lcp --proxyUrl https://www.yourdomain.ie
```

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
